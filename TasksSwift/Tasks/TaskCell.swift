//
//  TaskCell.swift
//  TasksSwift
//
//  Created by Jace on 8/5/18.
//  Copyright © 2018 Cultured Code GmbH & Co. KG. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    static let checkboxEmpty: UIImage = UIImage(named: "checkbox-empty")!
    static let checkboxChecked: UIImage = UIImage(named: "checkbox-checked")!
}

class TaskCell: UITableViewCell {
    
    var title: String? {
        get { return textLabel?.text }
        set { textLabel?.text = newValue }
    }
    private let checkboxImageView: UIImageView = UIImageView()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }
    
    private func setup() {
        checkboxImageView.image = UIImage(named: "checkbox-empty")
        contentView.addSubview(checkboxImageView)
        
        textLabel?.font = UIFont(name: "AmericanTypewriter", size: 16)
        textLabel?.textColor = UIColor.black
        
        selectionStyle = .none
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        checkboxImageView.sizeToFit()
        checkboxImageView.center = CGPoint(x: checkboxImageView.bounds.midX, y: checkboxImageView.bounds.midY)

        textLabel?.sizeToFit()
        textLabel?.center = CGPoint(x: checkboxImageView.frame.maxX + (textLabel?.bounds.midX ?? 0),
                                    y: bounds.midY)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        let image: UIImage = selected ? .checkboxChecked : .checkboxEmpty
        checkboxImageView.image = image
        
        textLabel?.textColor = selected ? .lightGray : .black
    }
}

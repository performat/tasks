//
//  TasksTableViewController.swift
//  TasksSwift
//
//  Created by Jace on 8/5/18.
//  Copyright © 2018 Cultured Code GmbH & Co. KG. All rights reserved.
//

import Foundation
import UIKit

class TasksTableViewController: UITableViewController {
    var tasks: [Task]
    private let feedbackGenerator: UISelectionFeedbackGenerator = UISelectionFeedbackGenerator()

    init(withTasks tasks: [Task]) {
        self.tasks = tasks
        super.init(style: UITableViewStyle.plain)

        title = "Tasks"
        toolbarItems = [UIBarButtonItem(title: "Complete All",
                                                  style: UIBarButtonItemStyle.plain,
                                                  target: self,
                                                  action: #selector(TasksTableViewController.completeAll)),
                             UIBarButtonItem(title: "Sort by name",
                                                  style: UIBarButtonItemStyle.plain,
                                                  target: self,
                                                  action: #selector(TasksTableViewController.sortByName))]
        
        tableView.register(TaskCell.self, forCellReuseIdentifier: TaskCell.reuseIdentifier)
        
        tableView.allowsMultipleSelection = true
        clearsSelectionOnViewWillAppear = false
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tasks.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: TaskCell = tableView.dequeueReusableCell(withIdentifier: TaskCell.reuseIdentifier, for: indexPath) as! TaskCell
        let task: Task = tasks[indexPath.row]
        
        cell.title = task.title
        cell.accessoryType = task.childrenTasks.count > 0 ? .detailDisclosureButton : .none
        
        return cell
    }

    // MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let task: Task = tasks[indexPath.row]
        if task.isCompleted {
            cell.isSelected = true
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }

    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        guard let cell: UITableViewCell = tableView.cellForRow(at: indexPath) else { return nil }
        
        feedbackGenerator.prepare()
        feedbackGenerator.selectionChanged()
        
        let task: Task = tasks[indexPath.row]
        
        if cell.isSelected {
            task.isCompleted = false
            cell.isSelected = false
            tableView.deselectRow(at: indexPath, animated: false)
            return nil
        } else {
            task.isCompleted = true
            return indexPath
        }
    }
    
    override func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        tasks[indexPath.row].isCompleted = false
        return indexPath
    }

    override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        let task: Task = tasks[indexPath.row]
        let vc: TasksTableViewController = TasksTableViewController(withTasks: task.childrenTasks)
        vc.title = task.title
        navigationController?.pushViewController(vc, animated: true)
    }

    @objc func completeAll() {
        tasks.enumerated().forEach { (offset, task) in
            task.isCompleted = true
            tableView.selectRow(at: IndexPath(row: offset, section: 0), animated: false, scrollPosition: .none)
        }
    }

    @objc func sortByName() {
        tasks.sort(by: { $0.title.localizedCaseInsensitiveCompare($1.title) == .orderedAscending })
        tableView.reloadSections(IndexSet(integer: 0), with: .automatic)
        tasks.enumerated().forEach { (offset, task) in
            if task.isCompleted {
                let indexPath: IndexPath = IndexPath(row: offset, section: 0)
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
        }
    }
}

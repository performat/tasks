//
//  AppDelegate.swift
//  TasksSwift
//
//  Created by Jace on 8/5/18.
//  Copyright © 2018 Cultured Code GmbH & Co. KG. All rights reserved.
//
import UIKit

let kTitle = "title"
let kCompleted = "completed"
let kChildren = "children"


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var tasksTableViewController: TasksTableViewController!
    var navigationController: UINavigationController!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        
        tasksTableViewController = TasksTableViewController(withTasks: _generateTasks())
        navigationController = UINavigationController(rootViewController: tasksTableViewController)
        navigationController.isToolbarHidden = false
        
        window?.rootViewController = navigationController
        window?.backgroundColor = UIColor.white
        window?.makeKeyAndVisible()

        return true
    }

    func _generateTasks() -> [Task] {
        let path: String = Bundle.main.path(forResource: "tasks", ofType: "json")!
        let url: URL = URL(fileURLWithPath: path)
        let data: Data = try! Data(contentsOf: url)
        let tasks: [Task]
        do {
            tasks = try JSONDecoder().decode([Task].self, from: data)
        } catch {
            print(error)
            tasks = []
        }
        return tasks
    }
}

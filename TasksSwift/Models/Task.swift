//
//  Task.swift
//  TasksSwift
//
//  Created by Jace on 8/5/18.
//  Copyright © 2018 Cultured Code GmbH & Co. KG. All rights reserved.
//

import Foundation

class Task: Equatable, Decodable {
    enum CodingKeys: String, CodingKey {
        case title
        case isCompleted = "completed"
        case childrenTasks = "children"
    }
    @objc var title: String {
        didSet {
            modified = Date()
        }
    }
    var isCompleted: Bool = false {
        didSet {
            modified = Date()
        }
    }
    var childrenTasks: [Task] = [] {
        didSet {
            modified = Date()
        }
    }
    var parentTask: Task?
    private(set) var modified: Date = Date()

    init(title: String?) {
        self.title = title ?? "<no title>"
    }
    
    required init(from decoder: Decoder) throws {
        let container: KeyedDecodingContainer<Task.CodingKeys> = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        isCompleted = try container.decode(Bool.self, forKey: .isCompleted)
        if container.contains(.childrenTasks) {
            childrenTasks = try container.decode([Task].self, forKey: .childrenTasks)
        }
    }

    func add(child: Task) {
        childrenTasks.append(child)
        child.parentTask = self
    }

    func remove(child: Task) {
        childrenTasks.removeAll(where: { $0 == child })
    }

    func deleteChildren() {
        childrenTasks.removeAll()
    }
}

func ==(lhs: Task, rhs: Task) -> Bool {
    return lhs.title == rhs.title &&
        lhs.isCompleted == rhs.isCompleted &&
        lhs.childrenTasks == rhs.childrenTasks &&
        lhs.modified == rhs.modified
}
